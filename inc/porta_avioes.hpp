#ifndef PORTA_AVIOES
#define PORTA_AVIOES

#include"navios.hpp"

using namespace std;

class Porta_Avioes : public Navios{
  private:
    bool habilidade_chance_de_destruir_missil;
    int vidas[4];
  public:
    void set_habilidade_chance_de_destruir_missil();
    bool get_habilidade_chance_de_destruir_missil();
    void set_vida(int i, int vida);
    int get_vida();

};

#endif
