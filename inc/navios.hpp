#ifndef NAVIOS
#define NAVIOS

#include<string>

using namespace std;

class Navios{
  private:
    int n_casas;
    int coordenada_x;
    int coordenada_y;
    string direcao;
  public:
    void set_n_casas(int n_casas);
    void set_coordenada_x(int coordenada_x);
    void set_coordenada_y(int coordenada_y);
    void set_direcao(string direcao);
    int get_n_casas();
    int get_coordenada_x();
    int get_coordenada_y();
    string get_direcao();
    Navios();
    ~Navios();
    Navios(int n_casas, int coordenada_x, int coordenada_y, string direcao);
};

#endif
