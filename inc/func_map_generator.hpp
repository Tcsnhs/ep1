#ifndef MAPA
#define MAPA
#include<iostream>
#include<string>
#include<vector>

using namespace std;

vector<vector<string>> mapa_inicial();
void cria_mapa(vector<vector<string>> map, string tipo_navio);
void limpa_tela();
vector<vector<string>> mapa_jogador(int n_navios, string tipo_navio, char vida, vector<vector<string>> mapa);


#endif
