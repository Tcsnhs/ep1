#include "func_map_generator.hpp"

using namespace std;

vector<vector<string>> mapa_inicial(){
  vector<vector<string>> map{ { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"},
                              { "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--", "--"} };

  return map;
}

void cria_mapa(vector<vector<string>> map1, string tipo_navio){
  vector<vector<string>> casa;
  casa = map1;
  for(int i=0; i<17; ++i){
    for(int j=0; j<16; ++j){
      if(i==0 && j==0){
        cout << "  ";
      }
      if(i==0 && j==1){
        cout << "[XX]==>";
      }
      if(i==1 && j==0){
        cout << "[YY]     ";
      }
      if(i>1 && i<3 && j==0){
        cout << " ||      ";
      }
      if(i==0 && j>=3 && j<12){
        cout << "[0"<< j-2 << "] ";
      }
      if(i==0 && j>=12){
        cout << "["<< j-2 << "] ";
      }
      if(i==1 && j>=3){
        cout << " ||  ";
      }
      if(i==2 && j>=3){
        cout << " \\/  ";
      }

      if(i>3 && j==0 && i<=12){
        cout << "[0"<< i-3 << "]" << "===> ";
      }
      if(j==0 && i>12){
        cout << "["<< i-3 << "]" << "===> ";
      }
      if(i>3 && j>=3 && casa[i-4][j-3]=="--"){
        cout << " [" << casa[i-4][j-3] << "]";
      }else if(i>3 && j>=3 && tipo_navio=="porta-avioes"){
        cout << " [" << casa[i-4][j-3] << "]";
      }else if(i>3 && j>=3 && tipo_navio=="submarino"){
        cout << " [" << casa[i-4][j-3] << "]";
      }else if(i>3 && j>=3 && tipo_navio=="canoa"){
        cout << " [" << casa[i-4][j-3] << "]";
      }
    }
    cout << endl;
  }
}
void limpa_tela(){
  for(int i=0; i<25; ++i){
    cout <<endl;
  }
}
vector<vector<string>> mapa_jogador(int n_navios, string tipo_navio, char vida, vector<vector<string>> mapa){

  string opcao="sim";
  string dados;
  char orientacao = 'c';
  int coordenadax;
  int coordenaday;
  int n_casas;
  int cont=0;
  bool choque_navios=false;
  char prefixo;

  if(tipo_navio=="porta-avioes"){
    n_casas = 4;
    prefixo='P';
  }else if(tipo_navio=="submarino"){
    n_casas = 2;
    prefixo='S';
  }else{
    n_casas = 1;
    prefixo='C';
  }
  n_casas-=1;
  dados.push_back(prefixo);
  dados.push_back(vida);

  if(tipo_navio=="Porta Avioes"){
    cout << "Navio: Porta-avioes" << endl << "Ocupa: 4 casas" << endl << "Habilidade: 35%% de chance de abater o missil antes de ser atingido" << endl << endl;
  }else if(tipo_navio=="Submarino"){
    cout << "Navio: Submarino" << endl << "Ocupa: 2 casas" << endl << "Habilidade: Cada casa precisa ser atingida 2 vezes para ser destruida" << endl << endl;
  }else if(tipo_navio=="Canoa"){
    cout << "Navio: Canoa" << endl << "Ocupa: 1 casa" << endl << "Habilidade: Nenhuma" << endl << endl;
  }
    cria_mapa(mapa, tipo_navio);

  while(n_navios!=0 || choque_navios==false){
    if(choque_navios==true){
      choque_navios=false;
      cout << "Infelizmente as coordenadas que voce escolheu resultaram em um choque" << endl << "Por favor selecione outra posicao" << endl;
    }else{
      if(cont==0){
        cont++;
      }else{
        n_navios--;
        if(n_navios==0){
          break;
        }
      }
    }

    cout << endl << "Voce possui " << n_navios << " " << tipo_navio << " para colocar no mapa" << endl;
    cout << "Digite o numero da linha do " << tipo_navio << ": ";
    cin >> coordenadax;

    cout << "Digite o numero da coluna do " << tipo_navio << ": ";
    cin >> coordenaday;

    if(tipo_navio!="Canoa"){
      cout << "Orientacoes disponibilizadas para escolha do jogador:" << endl;
      cout << "Digite E para esquerda" << endl << "Digite D para direita" << endl << "Digite C para cima" << endl << "Digite B para Baixo" << endl;
      cout << "Digite a orientacao do " << tipo_navio << ": ";
      cin >> orientacao;
      limpa_tela();
    }else{
      cout << "A canoa nao possui orientacao, logo ela recebe nada." << endl;
    }

    if(orientacao=='e' || orientacao=='E'){
      for(int j=0; j<n_casas+1; j++){
        if(coordenadax-1<0 || coordenaday-1-j<0 || coordenadax-1>=13 || coordenaday-1-j>=13){
          choque_navios=true;
        }else if(mapa[coordenadax-1][coordenaday-1-j]!="--"){
          choque_navios=true;
        }
      }
      if(choque_navios==false){
        for(int j=0; j<n_casas+1; j++){
            mapa[coordenadax-1][coordenaday-1-j]=dados;
        }
      }
    }else if(orientacao=='c' || orientacao=='C'){
      for(int i=0; i<n_casas+1; i++){
        if(coordenadax-1-i<0 || coordenaday-1<0 || coordenadax-1-i>=13 || coordenaday-1>=13){
          choque_navios=true;
        }else if(mapa[coordenadax-1-i][coordenaday-1]!="--"){
          choque_navios=true;
        }
      }
      if(choque_navios==false){
        for(int i=0; i<n_casas+1; i++){
          mapa[coordenadax-1-i][coordenaday-1]=dados;
        }
      }
    }else if(orientacao=='b' || orientacao=='B'){
      for(int i=0; i<n_casas+1; i++){
        if(coordenadax-1+i<0 || coordenaday-1<0 || coordenadax-1+i>=13 || coordenaday-1>=13){
          choque_navios=true;
        }else if(mapa[coordenadax-1+i][coordenaday-1]!="--"){
          choque_navios=true;
        }
      }
      if(choque_navios==false){
        for(int i=0; i<n_casas+1; i++){
          mapa[coordenadax-1+i][coordenaday-1]=dados;
        }
      }
    }else if(orientacao=='d' || orientacao=='D'){
      for(int j=0;j<n_casas+1; j++){
        if(coordenadax-1<0 || coordenaday-1+n_casas-j<0 || coordenadax-1>=13 || coordenaday-1+n_casas-j>=13){
          choque_navios=true;
        }else if(mapa[coordenadax-1][coordenaday-1+n_casas-j]!="--"){
          choque_navios=true;
        }
      }
      if(choque_navios==false){
        for(int j=0;j<n_casas+1; j++){
            mapa[coordenadax-1][coordenaday-1+n_casas-j]=dados;
        }
      }
    }
    limpa_tela();
    cria_mapa(mapa, tipo_navio);
  }
  return mapa;
}
