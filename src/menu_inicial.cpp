#include <iostream>
#include <string>
#include <cstdlib>
#include "menu_inicial.hpp"
#include "func_map_generator.hpp"

using namespace std;

string func_nick(int n_jogador){
  char conf='S';
  string jogador;
  do{
      cout << "Jogador " << n_jogador << " escolha um nome de jogo de no maximo uma palavra: ";
      cin >> jogador;
      limpa_tela();
      cout << "Jogador " << n_jogador << " se chama: " << jogador ;
      cout << endl <<"Tem certeza deste nome?[s/n]:" << endl;
      cin >> conf;
      cout << endl;
      limpa_tela();
    }while((conf!='s')&&(conf!='S'));
  return jogador;
}

int func_n_navios(string tipo_navio, int n_max_navios){
  int n_navios=0;
  char conf='S';
  do{
    do{
      if(n_navios>n_max_navios){
        limpa_tela();
        cout << "Numero invalido, por favor respeitar o limite estabelecido" << endl;
      }
      cout << "Numero maximo de " << tipo_navio << " disponiveis: " << n_max_navios << endl;
      cout << "Digite o numero de " << tipo_navio << " escolhido: ";
      cin >> n_navios;
    }while(n_navios>n_max_navios);
    limpa_tela();
    cout << "Numero de "<< tipo_navio << " escolhido: " << n_navios << endl;
    cout << "Voces tem certeza sobre este numero?[s/n]" << endl;
    cin >> conf;
    limpa_tela();
  }while(conf!='s'&&conf!='S');
  return n_navios;
}
