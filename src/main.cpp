#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cstdlib>
#include "menu_inicial.hpp"
#include "func_map_generator.hpp"

using namespace std;

int main(int argc, char ** argv){
  vector<vector<string>> mapa_completo;
  string jogador1;
  string jogador2;
  int n_porta_avioes;
  int n_submarinos;
  int n_canoas;
  int opcao;

  mapa_completo=mapa_inicial();

  cout << endl <<"Bem vindos ao jogo de Batalha Naval" << endl << endl;
  cout << "Primeiro voces devem escolher como querem ser chamados" << endl << endl;

  jogador1=func_nick(1);
  jogador2=func_nick(2);

  cout << "Agora voces podem escolher entre utilizar um mapa ja preparado ou criar o mapa voces mesmos." << endl;
  cout << "Digite 1 para criar um mapa e 2 para utilizar um mapa ja preparado: ";
  cin >> opcao;
  limpa_tela();
  
  if(opcao==1){
    cout << "Pronto agora voces precisam entrar em um consenso sobre quantos navios de cada\ntipo cada um de voces podera ter." << endl << endl;

    n_porta_avioes=func_n_navios("Porta Avioes", 12);
    n_submarinos=func_n_navios("Submarinos", 24);
    n_canoas=func_n_navios("Canoas", 36);

    cout << "Agora " << jogador1 << " ira escolher a posiçao dos seus navios.\n" << jogador2 << " por favor se retire enquanto ele realiza sua escolha." << endl;
    cout << "Quando estiver pronto, pressione qualquer tecla." << endl;
    getchar();
    limpa_tela();

    mapa_completo=mapa_jogador(n_porta_avioes, "porta-avioes", '1', mapa_completo);
    mapa_completo=mapa_jogador(n_submarinos, "submarino", '2', mapa_completo);
    mapa_completo=mapa_jogador(n_canoas, "canoa", '1', mapa_completo);
    limpa_tela();
  }
  if(opcao==2){

  }
    //cout << endl << endl << "Começaremos com os barcos maiores e depois iremos aos menores" << endl;

    return 0;
}
