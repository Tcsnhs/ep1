#include "navios.hpp"
#include <iostream>

void Navios::set_n_casas(int n_casas){
  this->n_casas=n_casas;
}
void Navios::set_coordenada_x(int coordenada_x){
  this->coordenada_x=coordenada_x;
}
void Navios::set_coordenada_y(int coordenada_y){
  this->coordenada_y=coordenada_y;
}
void Navios::set_direcao(string direcao){
  this->direcao=direcao;
}
int Navios::get_n_casas(){
  return n_casas;
}
int Navios::get_coordenada_x(){
  return coordenada_x;
}
int Navios::get_coordenada_y(){
  return coordenada_y;
}
string Navios:: get_direcao(){
  return direcao;
}
Navios::Navios(){
  set_n_casas(0);
  set_coordenada_x(-1);
  set_coordenada_y(-1);
  set_direcao(" ");
}
Navios::~Navios(){}
Navios::Navios(int n_casas, int coordenada_x, int coordenada_y, string direcao){
  set_n_casas(n_casas);
  set_coordenada_x(coordenada_x);
  set_coordenada_y(coordenada_y);
  set_direcao(direcao);
}
